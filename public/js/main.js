$(document).ready(function() {
console.log("load main.js");
/**
*  regex
*/
        var regex = {

               regexInput: /^[A-Za-z ]{3,}$/,
               regexEmail: /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i,
               regexNumber: /^[0-9]{2}$/,
               regexPass: /^[A-Za-z0-9!#$%&*?@]{3,}$/,
               /**
                * [validation input]
                */
               valideInputField: function(input){

                        // if(!this.regexInput.test(input)){
                        //   alert('"' + input + '"' + ' format is not correct !')
                        //   return false;
                        // } else {
                        //   return true;
                        // }
                        return true
                },
                valideInputEmail: function(email){
                        //
                        // if(!this.regexEmail.test(email)){
                        //   alert('email format is not correct!')
                        //   return false;
                        // } else {
                        //   return true;
                        // }
                        return true
                },
                valideInputPassword: function(password){
                          // if(!this.regexPass.test(password)){
                          //   alert('PASSWORD format is not correct!\nplease use only:\n- letters (CaSe SeNsItIvE)\n- numbers\n- spec chars: !#$%&*?@ ')
                          //   return false;
                          // } else {
                          //   return true;
                          // }
                          return true
                }
                // ,
                // valideInputNumber: function(num){
                //
                //       if(num != '' || (num.length > 1 && num.length < 3) || (num > 7 && num < 77)){
                //             if(!this.regexNumber.test(num)){
                //               alert('age non valide !')
                //               return false;
                //             } else {
                //               return true;
                //             }
                //       }
                // }
        };


/**
* check input inscription
*/
          function valideRegex() {

              var lastname  = $('#signup #lastname').val();
              var firstname = $('#signup #firstname').val();
              var email     = $('#signup #email').val();
              var nickname  = $('#signup #nickname').val();
              var password  = $('#signup #password').val();

              console.log("VALIDEREGEX: ", lastname, firstname, nickname, email, password);

             /**
              *  vars
              */
              var valideEmail     = regex.valideInputEmail(email);
              var valideLastname  = regex.valideInputField(lastname);
              var valideFirstname = regex.valideInputField(firstname);
              var validePass      = regex.valideInputPassword(password);

              /**
               * if nom & prenom ok => email validation => submit form
               */
              if( !valideLastname || !valideFirstname || !validePass){
                return;
              } else {
                          if( !valideEmail ){

                              //alert('email pas bon')
                              return;
                          } else {
                            $.ajax({
                              type: "POST",
                              url: '/signup',
                              data: {
                                "email"    : email,
                                "lastname" : lastname,
                                "firstname": firstname,
                                "nickname" : nickname,
                                "password" : password
                              },
                              success: function(data, textStatus, jqXHR) {
                                console.log(data);
                                if(data.error){
                                          if( !$('.error').is(':visible') ){
                                            var error = '<div class="error">' + data.error + ' </div>';
                                            $('#form-container').append(error);

                                            // remove error after 5s
                                            setTimeout(function(){
                                              $('.error').remove();
                                            },5000)
                                          }
                                        } else {
                                          console.log("data.redirect");
                                          window.location = data.redirect
                                      }
                                    }
                            });
                      }// end if
              }

          };// end ivalideRegex


      $('#submit-form').submit(function(event) {
        event.preventDefault();
        valideRegex()
      });


/**
* ckeck input login
*/

             function checkRegLogin() {

                 var login    = $('#signin .login').val();
                 var password = $('#signin #password').val();

                 /**
                  *  vars
                  */
                  // var valideEmail      = regex.valideInputEmail(email);
                  var valideLogin      = regex.valideInputField(login)
                  var validePassword   = regex.valideInputField(password);

                 /**
                  * if nom ok => email ok => login
                  */
                 if( !valideLogin ){
                   return;
                 } else {

                               $.ajax({
                                 type: "POST",
                                 url: '/login',
                                 data: {"login": login, "password": password },
                                 success: function(data, textStatus, jqXHR) {
                                        /**
                                         * if error display error message
                                         */
                                        if(data.error){
                                        console.log('data.error:: ', data);
                                          if( !$('.error').is(':visible') ){
                                            var error = [
                                                      '<div class="form-group clearfix">',
                                                          '<div  class="row">',
                                                          '<div for="" class="col-sm-3  "></div>',
                                                              '<div class=" col-sm-7 ">',
                                                               '<div class="col-xs-12 well error">' + data.error + ' </div>',
                                                              '</div>',
                                                           '</div>',
                                                          '</div>'
                                                        ]

                                            $( '#form-container #signin' ).append( error.join("") );
                                            $('.error').hide().slideToggle();

                                            // remove error after 5s
                                            setTimeout(function(){
                                               $('.error').slideToggle(function(){ $( this ).remove()});
                                            },5000)
                                          }

                                        }

                                        if(data.user){
                                          window.location = data.redirect;
                                        //  console.log('DATA.USER:: ', data.email);
                                        }

                                      }
                               });

                         }// end if

  }



      $('#submit-login').submit(function(event) {
        event.preventDefault();
         checkRegLogin();
      });

/**
  *  Send Password
  */
  function valideRegResetPwd() {


      var email     = $('#submit-forgot-password #email').val();
     /**
      *  vars
      */
      var valideEmail     = regex.valideInputEmail(email);
      /**
       * email validation => submit form
       */
      if( !valideEmail ){
        return;
      } else {
                    $.ajax({
                      type: "POST",
                      url: '/request-password',
                      data: {
                        "email"    : email
                      },
                      success: function(data, textStatus, jqXHR) {

                        if(data.error){
                                  if( !$('.error').is(':visible') ){
                                    var error = '<div class="error">' + data.error + ' </div>';
                                    $('#form-container').append(error);

                                    // remove error after 5s
                                    setTimeout(function(){
                                      $('.error').remove();
                                    },5000)
                                  }
                                } else {
                                //  window.location = data.redirect
                              }
                            }
                    });
      }

  };// end ivalideRegex

      $('#submit-send-password').submit(function(event){
        event.preventDefault();
         valideRegResetPwd();
      })

})
