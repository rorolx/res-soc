/**
 * focus input field
 */
$(document).ready(function(){

  /* focus */
  var placeholder = {
    current   : undefined,
    email     : 'Email',
    firstname : 'First Name',
    lastname  : 'Last Name',
    password  : 'Password',
    nickname  : 'Nickname',
    register  : 'Register',
    signin    : 'Sign In',
    login     : 'Email or Nickname',
    logout    : 'Log Out',
    resetpwd  : 'Reset password',
    edit      : 'Edit',
    update    : 'Update',
    cancel    : 'Cancel'
  };

  function placeholerInput(){
      $('.focus .email').attr('placeholder', placeholder.email);
      $('.focus .firstname').attr('placeholder', placeholder.firstname);
      $('.focus .lastname').attr('placeholder', placeholder.lastname);
      $('.focus .nickname').attr('placeholder', placeholder.nickname);
      $('.focus .password').attr('placeholder', placeholder.password);
      $('.resetPwd').attr('placeholder', placeholder.resetpwd);
      $('.register').attr('placeholder', placeholder.register);
      $('.signin').attr('placeholder', placeholder.signin);
      $('.login').attr('placeholder', placeholder.login);
      $('.logout').attr('placeholder', placeholder.logout);
      $('.edite').attr('placeholder', placeholder.edit);
      $('.update').attr('placeholder', placeholder.update);
      $('.cancel').attr('placeholder', placeholder.cancel);
  };
  placeholerInput();

  $('.focus input').focusin(function(){
      placeholder.current = $(this).attr('id');
      $(this).attr('placeholder','');
  });

  $('.focus input').focusout(function(){
      var id = placeholder.current;
      if($(this).val() == '' ){
            $(this).attr('placeholder', placeholder[id]);
      };
  });


})// end doc ready
