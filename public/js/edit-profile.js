// edit-profile
 $(document).ready(function(){

/**
 * load user-profile
 */
            // hide user-container before content loaded
            $('.user-container').hide();
            // load info-profile. If success slideDown user-container
            $('.user-info').load('info-profile', function() {
                  $('.user-container').slideDown();
            });

/**
 * modifier
 */
          $(document).on('click', '.modifier button', function (event) {
            event.preventDefault();

            $('.user-container').slideUp(function(){
                        $('.user-info').load('edit-profile', function() {
                          $('.user-container').slideDown();
                        });
            });
          });
/**
 * annuler
 */
          $(document).on('click', '.annuler button', function (event) {
            event.preventDefault();

            $('.user-container').slideUp(function(){
                        $('.user-info').load('info-profile', function() {
                          $('.user-container').slideDown();
                        });
            });

          });

  })// end doc ready
