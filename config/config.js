
module.exports = function(){
	var config = {
		port: 3333, 
		db: 'mongodb://localhost:27017/social',
	};
	// override some globals
	process.env.PORT = config.port;
	return config;
}();
