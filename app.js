/**
 * Module
 */

var util             = require("util");
var express          = require('express');
var fs               = require('fs');
var cookieParser     = require('cookie-parser');
var compress         = require('compression');
var session          = require('express-session');
var bodyParser       = require('body-parser');
var logger           = require('morgan');
var errorHandler     = require('errorhandler');
var methodOverride   = require('method-override');
var assert           = require("assert");
var _                = require('lodash');
var flash            = require('express-flash');
var path             = require('path');
var colors           = require('colors');
var expressValidator = require('express-validator');
var connectAssets    = require('connect-assets');
var request          = require("request");
// var search       = require('shared/search.js');
var mongoose         = require('mongoose');
mongoose.Promise = require('bluebird');
var uuidv1           = require('uuid/v1');
var multer           = require('multer');
var storage          = multer.diskStorage({
    destination: function(req, file, callback){
        callback(null, './public/img/profile'); // set the destination
        console.log(colors.cyan.bold("FILENAME"), file);
    },
    filename: function(req, file, callback){
        callback(null, uuidv1() +'.jpg'); // set the file name and extension
    }
});
var upload = multer({storage: storage});

//////////////////////////////////////////////////////////
// var server           = require('http').createServer(app);
// var io               = require('socket.io')(server);
//////////////////////////////////////////////////////////
/**
 * Configuration.
 */
var config = require('./config/config.js');

/**
 * Create Express server.
 */
var app = express();

/**
 * Express configuration.
 */

app.set('port', process.env.PORT);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');


var domain = require('domain');

app.use(function(req, res, next) {
    var requestDomain = domain.create();
    requestDomain.add(req);
    requestDomain.add(res);
    requestDomain.on('error', next);
    requestDomain.run(next);
});

app.use(compress());
app.use(connectAssets({
  paths: [path.join(__dirname, 'public/css'), path.join(__dirname, 'public/js')]
}));

app.use(logger('dev',{
	skip: function(req, res){
		function inDir(dirNameList){
			var pattern = "^\/"+dirNameList[0]+"\/";
			for(var i=1; i<dirNameList.length; i++){
				pattern += "|^\/"+dirNameList[i]+"\/";
			}
			return pattern;
		}
		var exclude = new RegExp(inDir(["css","js","img", "images", "fonts"]));
		return exclude.test(req.path);
	}
}));
app.use(bodyParser.json({limit:'3mb'}));
app.use(bodyParser.urlencoded({ extended: true }));


app.use(expressValidator());
app.use(methodOverride());
app.use(cookieParser());
app.use(session({
  resave: true,
  saveUninitialized: true,
  secret: "myawesomesecrets"
}));

app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

app.use(flash());


app.use(function(req, res, next) {
  // Remember original destination before login.
   if (/css|fonts|images|img|js/i.test(path)){
    return next();
  }
  if(req.session) {
    req.session.returnTo = req.path;
  }
  next();
});

app.use(express.static(path.join(__dirname, 'public'), { maxAge: 31557600000 }));

/**
 * Controllers (route handlers).
 */

var demoController = require('./controllers/demoController');
/**
 * Main routes.
 */

app.post("/", demoController.postIndex);

// redirect default index
app.get("/", demoController.getIndex);

app.get('/user/:name',  demoController.getUser);

// inscripion
app.get("/signup", demoController.getSignup);
app.post("/signup", demoController.postSignup);

// login
app.get("/login", demoController.getLogin);
app.post("/login", demoController.postLogin);

// send password
app.get('/forgot-password', demoController.getForgotPassword);
app.post('/forgot-password', demoController.postForgotPassword);

// request password
app.get('/request-password', demoController.getRequestPassword);
app.post('/request-password', demoController.postRequestPassword);

// mon compte
app.get("/profile", function(req, res, next){
      if(!req.session.email ){
        return res.redirect('/');
      } else {
        return next()
      }
},  demoController.getProfile);



app.post("/profile", function(req, res, next){
      if(!req.session.email ){
        return res.redirect('/');
      } else {
        return next()
      }
}, demoController.postProfile);

// deconnexion
app.get('/logout', function(req, res, next){
      if(!req.session.email ){
        return res.redirect('/');
      } else {
        return next()
      }
}, demoController.getLogout);

// info profile
app.get('/info-profile', function(req, res, next){
      if(!req.session.email ){
        return res.redirect('/');
      } else {
        return next()
      }
}, demoController.getInfoProfile);

// edit get profile
app.get('/edit-profile', function(req, res, next){
      if(!req.session.email ){
        return res.redirect('/');
      } else {
        return next()
      }
}, demoController.getEditProfile);

// edit post profile
app.post('/edit-profile', upload.single('avatar'), function(req, res, next) {
      var image = req.file.filename;
    if(!req.session.email ){
            return res.redirect('/');
          } else {
            return next()
          }
    }, demoController.postUpdateProfile);


// get board
app.get('/board', function(req, res, next){
      if(!req.session.email ){
        return res.redirect('/');
      } else {
        return next()
      }
}, demoController.getBoard);

app.post('/board',  function(req, res, next){
      if(!req.session.email ){
        return res.redirect('/');
      } else {
        return next()
      }
}, demoController.postBoard);

app.post('/delete-message',  function(req, res, next){
      if(!req.session.email ){
        return res.redirect('/');
      } else {
        return next()
      }
}, demoController.postDeleteMessage);

//get members
app.get('/members', function(req, res, next){
      if(!req.session.email ){
        return res.redirect('/');
      } else {
        return next()
      }
}, demoController.getMembers);


//friend-request
app.post('/friend-request',  function(req, res, next){
      if(!req.session.email ){
        return res.redirect('/');
      } else {
        return next()
      }
}, demoController.postFriendRequest);

app.get('/friend-request',  function(req, res, next){
      if(!req.session.email ){
        return res.redirect('/');
      } else {
        return next()
      }
}, demoController.getFriendRequest);

//friend-request
app.post('/response-friend-request',  function(req, res, next){
      if(!req.session.email ){
        return res.redirect('/');
      } else {
        return next()
      }
}, demoController.postResponseFriendRequest);

/**
 * search
 */
 app.post('/search',  function(req, res, next){
       if(!req.session.email ){
         return res.redirect('/');
       } else {
         return next()
       }
 }, demoController.postSearch);


/**
 * 500 Error Handler.
 */
app.use(errorHandler());

// Handle 404
app.use(function(req, res) {
    res.status(400);
    res.render('404.ejs', {title: '404: File Not Found'});
});

/**
 * Start Express server.
 */
app.listen(app.get('port'), function() {

  console.log(colors.green.bold('Express server listening on port %d in %s mode'), app.get('port'), app.get('env') );

});


module.exports = app;
