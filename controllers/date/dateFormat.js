var request = require("request");
var moment = require("moment");
var util = require("util");
var fs = require('fs');

module.exports.getDate = function() {

    var date = new Date();
    var dd = date.getDay();
    var mm = date.getMonth();
    var yyyy = date.getFullYear();
    var h = date.getHours();
    var m = date.getMinutes();
    var s = date.getSeconds();

    if (dd < 10) dd = '0' + dd;
    if (mm < 10) mm = '0' + mm;
    if (h < 10) h = '0' + h;
    if (m < 10) m = '0' + m;
    if (s < 10) s = '0' + s;

    var date = dd + '/' + mm + '/' + yyyy + '-' + h + ':' + m + ':' + s;
    console.log('DATE.JS', dd, mm, yyyy);
    return date
};