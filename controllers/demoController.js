
var request      = require("request");
var moment       = require("moment");
var util         = require("util");
var fs           = require('fs');
var url          = require('url');
var session      = require('express-session');
var sanitize     = require('sanitize-html');
var colors       = require('colors');
var validator    = require('validator');
var nodemailer   = require('nodemailer');
var async        = require('async');
var bcrypt       = require('bcrypt');
var dateFormat   = require('./date/dateFormat.js');
var uuidv1       = require('uuid/v1');
var uuidv4       = require('uuid-v4');
// var search       = require('./shared/search.js');
var BCRYPT_SALT  = 10;

/////////////////////////// mongoose ///////////////////////////

var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var UserSchema = new Schema({
  uid            : String,
  email          : { type: String, required: true, unique: true},
  lastname       : { type: String, trim: true},
  firstname      : { type: String, trim: true},
  nickname       : { type: String, trim: true, unique: true},
  password       : String,
  avatar         : String,
  dob            : String,
  gender         : String,
  details        : String,
  presentation   : String,
  date           : String,
  role           : String,
  friends        : [String],
  messages_board:  {
      message: {type: String},
	    user   : {type: String},
      	dateGT : {type: Date},
		date   : { type: String },
      	uuid   : {type: String}
  },
  friend_request: {
	user: { type:String },
	date: { type: Date },
	uid : { type: String }
},
  messages_users : [String],
  chat           : [String]
}, { collection: 'users' });

UserSchema.pre('save', function(next) {
    var user = this;
    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next();

    // generate a salt
    bcrypt.genSalt(BCRYPT_SALT, function(err, salt) {
        if (err) return next(err);

        // hash the password using our new salt
        bcrypt.hash(user.password, salt, function(err, hash) {
            if (err) return next(err);

            // override the cleartext password with the hashed one
            user.password = hash;
            next();
        });
    });
});

UserSchema.methods.comparePassword = function(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};

var Users = mongoose.model('Users', UserSchema);

mongoose.connect('mongodb://localhost/social');

var db = mongoose.connection;
  db.on('error', console.error.bind(console, 'connection error:'));

  db.once('open', function() {
   console.log(colors.green.bold('mongoose connecté '));
});

////////////////////////////////////////////////////////////////

var notifications = {
	friendRequest: [],
	messageUsers : []
}

/**
 * Index, render search form
 **/
exports.getIndex = function(req, res, next) {
    return res.render("index.ejs", {
        error: ""
    });
};

/**
 * Index post search
 **/
exports.postIndex = function(req, res, next) {
  return res.render("index.ejs", {
      error: ""
  });
};


exports.getUser = function(req, res, next){

	if( req.session.email ){

		if ( req.params.name === req.session.nickname){
			return res.redirect('/profile');
		} else {
      ////////////////////
            Users.findOne({

               'nickname'    : req.params.name }, function(err, data) {
                 if(err) throw err;

                 if (data) {

console.log('********************************************');
console.log('********************************************');
console.log('NAME:: ', req.params);
console.log("USERDATA:: ", data);
console.log("USERDATA:: ", data.friends);

	if ( data.friends.length && typeof(data.friends) !=='undefined'){

							data.friends.forEach( function(val, i){

								console.log("DATAUSEREACH:: ", val );
								console.log("DATAUSEREACHLENGTH:: ", data.friends.length );

									if ( val.indexOf( req.session.nickname) > -1 ){
								console.log("OK AMI");
										  return res.render("user.ejs", {
							                       error        : "",
							                       name         : req.params.name,
												   user         : req.session,
												   friends      : data.friends,
												   board        : data.messages_board,
												   friendRequest: notifications.friendRequest
							                   });
									} else {
										console.log("PAS AMI");
											//return res.redirect(200, '/' );
									}
							})

	} else {
		console.log("PAS AMI2");
		//   	return res.redirect('/members');
		return res.redirect(url.format({
			pathname:'/members',
			user : req.params.name
		}))

	}

	console.log('********************************************');
	console.log('********************************************');

                 }
            });
		}

    } else {
      return res.redirect('/');
    }
};

/**
 * getSignup
 **/
exports.getSignup = function(req, res, next) {
    if( !req.session.email ){
      return res.render("signup.ejs", {
          error: ""
      });
    } else {
      res.redirect('/profile');
    }
};
/**
 * postInscripion
 **/
exports.postSignup = function(req, res, next) {

      /**
       * vars
       */
        var email     = sanitize( req.body.email );
        var lastname  = sanitize( req.body.lastname );
        var firstname = sanitize( req.body.firstname );
        var nickname  = sanitize( req.body.nickname );
        var dob       = sanitize( req.body.dob ) || null; ///////////////////////////undefined to remove
        var gender    = sanitize( req.body.gender ) || null; //////////////////////////////// to remove
        var password  = sanitize( req.body.password );
        var avatar    = 'rsz_default_avatar.png';
        var date      = new Date().getTime();
		var uid       = uuid();
        // role : M = Member, A = Admin, SPA = SuPer Admin
        var role      = 'M';
/*
moment.locale('fr');
console.log('MO:: ',moment(date).format('L'));
*/
        Users.findOne({

           'email'    : email,
           'firstname': firstname,
           'lastname' : lastname }, function(err, user) {

             if(err){
               return res.send({ error: 'An error has occured'});
             }
             if (user) {
                return res.send({ error: 'utilisateur deja enregistré!' });
             } else {
                return addUserInDatabase();
             }
        });
        /**
         *  function check if user already exists
         */
             function addUserInDatabase(){

               var user_data = {
				   				   uid            : uid,
                                   email          : email,
                                   lastname       : lastname,
                                   firstname      : firstname,
                                   nickname       : nickname,
                                   dob            : dob,
                                   gender         : gender,
                                   password       : password,
                                   avatar         : avatar,
                                   date           : date,
                                   role           : role,
                                   friends        : [],
                                   messages_board : [],
                                   messages_users : [],
								   friend_request : [],
                                   chat           : []
                                };

              var user = new Users(user_data);

              user.save(function(err, results){
                  if(err) {
                      // 404
                      // return res.redirect('/');
                    return res.send({error:'error'})
                  } else {
                    console.log("redirect profile");
                     return res.send({ redirect: '/login' });
                  }
                })
            };
};

/**
 * Send password
 **/
exports.getForgotPassword = function(req, res, next) {
  if( !req.session.email ){
    return res.render("forgot-password.ejs", {
        error: ""
    });
  } else {
    return res.redirect('/profile');
  }
};
exports.postForgotPassword = function(req, res, next) {
  if( !req.session.email ){
    return res.render("forgot-password.ejs", {
        error: ""
    });
  } else {
    return res.redirect('/profile');
  }
};

/**
 *  request password
 */
exports.getRequestPassword = function(req, res, next) {
   if( !req.session.email ){
     return res.redirect('/');
   } else {
     return res.redirect('/profile');
   }
}

 exports.postRequestPassword = function(req, res, next) {
   if( !req.session.email ){
//--------------------//
        var email = sanitize( req.body.email );

        async.waterfall([
            function(done) {
              crypto.randomBytes(20, function(err, buf) {
                var token = buf.toString('hex');
                done(err, token);
              });
            },
            function(token, done) {
              User.findOne({ email: email }, function(err, user) {
                if (!user) {

                 return res.send({ error: 'Email does not belong to any account !' });
                }

                user.resetPasswordToken = token;
                user.resetPasswordExpires = Date.now() + 3600000; // 1 hour

                user.save(function(err) {
                  done(err, token, user);
                });
              });
            },
            function(token, user, done) {
              //-----------------------//
              var smtpTransport = nodemailer.createTransport('SMTP', {
                service: 'gmail',
                auth: {
                  user: '!!! YOUR gmail USERNAME !!!',
                  pass: '!!! YOUR gmail PASSWORD !!!'
                }
              });
              var mailOptions = {
                to: user.email,
                from: 'passwordreset@demo.com',
                subject: 'Password Reset',
                text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
                  'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
                  'http://' + req.headers.host + '/reset/' + token + '\n\n' +
                  'If you did not request this, please ignore this email and your password will remain unchanged.\n'
              };
              smtpTransport.sendMail(mailOptions, function(err) {
                req.flash('info', 'An e-mail has been sent to ' + user.email + ' with instructions.');
                done(err, 'done');
              });
              //--------------------//
            }
          ], function(err) {
            if (err) return next(err);
            res.redirect('/forgot-password');
          });

//--------------------//
     var email = sanitize( req.body.email );
     Users.findOne({
        'email'    : email }, function(err, user) {
          if(err) throw err;
          if (user) {
                 //////////////////////////// EMAIL //////////////////////////////
                 //////////////////////////// ////////////////////////////////////

                    console.log('USer is in DB');


                 /////////////////////////////////////////////////////////////////
                 /////////////////////////////////////////////////////////////////
          } else {
           return res.send({ error: 'Email does not belong to any account !' });
          }
        });
//----------------//
   } else {
     return res.redirect('/profile');
   }

}


/**
 * Login
 **/
exports.getLogin = function(req, res, next) {
  if( !req.session.email ){
    return res.render("login.ejs", {
        error: ""
    });
  } else {
    return res.redirect('/');
  }
};

exports.postLogin = function(req, res) {

  /**
   * vars
   */
    // var email    = sanitize( req.body.email );
    // var nickname   = sanitize( req.body.nickname );
    var login    = sanitize( req.body.login );
    var password = sanitize( req.body.password );
    var email;
    var nickname;
    var lastname;
    var firstname;

    console.log(colors.cyan.bold( 'LOGIN:: ', login, 'EMAIL:: ', email, 'nickname:: ', nickname) );

    if ( validator.isEmail(login) ) {
      email = login;
    } else {
      nickname = login;
      console.log('nickname::', nickname);
    };


    console.log(colors.cyan.bold("REQ.BODY:: "), req.body);

          Users.findOne(
          {
                $or: [
                       { 'email' : email },
                       { 'nickname': nickname }
                     ]
            }, function(err, data) {

                      if(err){         throw err;         };

                      if(!data){
                         console.log(colors.red.bold('User is not in DB => FALSE'));
                        ///////////////  res.send error message //////////////
                        return res.send({ error: 'Bad Login or Password' });
                        //////////////////////////////////////////////////////
                      }

                      if (data) {
                        console.log(colors.cyan.bold('User is in DB => TRUE'));
                        ////////////////////////////////////////////////////////////////////////
                        /////////////////////////// COMPARE PASSWORD ///////////////////////////

                        data.comparePassword( req.body.password, function(err, isMatch) {

                           if ( err ) {      throw err;          };

                           if(!isMatch){
                             ///////////////  res.send error message //////////////
                             return res.send({ error: 'Bad Login or Password' });
                             //////////////////////////////////////////////////////
                           }

                           if( isMatch ) {
                                    /**
                                     * set user data's
                                     */
                                      _id          = data['_id'];
                                      email        = data['email'];
                                      firstname    = data['firstname'];
                                      lastname     = data['lastname'];
                                      nickname     = data['nickname'];
                                      dob          = data['dob'];
                                      gender       = data['gender'];
                                      details      = data['details'];
                                      presentation = data['presentation'];
                                      avatar       = data['avatar'];
                                      role         = data['role'];
									  uid          = data['uid'];

                                      /**
                                      * set user session
                                      */
                                      req.session._id          = _id;
                                      req.session.email        = email;
                                      req.session.firstname    = firstname;
                                      req.session.lastname     = lastname;
                                      req.session.nickname     = nickname;
                                      req.session.dob          = dob;
                                      req.session.gender       = gender;
                                      req.session.details      = details;
                                      req.session.presentation = presentation;
                                      req.session.avatar       = avatar;
                                      req.session.role         = role;
									  req.session.uid          = uid;




									/********* NOTIFICATIONS ********/
									//   var friendRequest = JSON.parse(JSON.stringify( data.friend_request ) ) ;
									//
									//   	console.log("PARSE:: ", friendRequest);
									// 	console.log("PARSE.LENGTH:: ", friendRequest.length);
									//
									// 	try{
									// 		for(var i in parse){
									// 			console.log('FORVAR:: ', parse[i].uid);
									// 		}
									// 	}catch(err){}
								  	//notifications.friendRequest = JSON.parse(JSON.stringify( data.friend_request ) )  ;

									var getFriendRequestUID = JSON.parse(JSON.stringify( data.friend_request ) )  ;
									var arrayUID = [];
									for( var i in getFriendRequestUID){
										console.log( "getFriendRequestUID:: " , getFriendRequestUID[i].uid);
										arrayUID.push( getFriendRequestUID[i].uid );
										console.log( "array-UID:: " , arrayUID);
									}

									Users.aggregate( [
											{
												$project :
											  {  _id: 0,  uid: 1  }
											}
									])
									.exec(function(err, results){
											console.log("UID-aggregate:: ", results);
											var data = JSON.parse( JSON.stringify(results));

											data.forEach( function(val, i){
												console.log("VALEACHAGGREGATE:: ", val);
												console.log("VALEACHAGGREGATE:: ", val.uid);

											})

									});


									 notifications.friendRequest = data.friend_request;

										console.log("NOTIFI:: ", notifications.friendRequest);
										console.log("NOTIFI-UID:: ", notifications.friendRequest.uid);

										try{
											console.log("NOTI.LEGNTH::", notifications.friendRequest.length);
										}catch(err){

										}

                                      console.log(colors.cyan.bold('///// SESSION /////'));
                                      console.log('SESSION:: ', req.session);
                                      console.log(colors.cyan.bold('///////////////////'));

                                      ////////  res.send redirect /profil + req.session ///////
                                      return res.send({
										  redirect: '/board',
										  user: req.session,
									      friendRequest: notifications.friendRequest
									  });

                                      console.log("REQ.SESSION.EMAIL::", req.session.email);
                                      /////////////////////////////////////////////////////////
                           }
                       });
                     }
          }); //end mongoose
};

/**
 * Board
 **/
exports.getBoard = function(req, res, next) {
  if( req.session.email ){
    ////////////////////

          Users.findOne({

             'email'    : req.session.email }, function(err, data) {
               if(err) throw err;

               if (data) {
				 console.log(colors.magenta.bold('////////// getBoard /////////////'));
                 console.log(colors.magenta.bold('DATA:: '), data);
				 console.log(colors.magenta.bold('/////////////////////////////////'));

                 return res.render("board.ejs", {
                     error        : "",
                     user         : req.session,
                     friends      : data.friends,
                     board        : data.messages_board,
                     uuid         : data.uuid,
					 messages     : data.messages_users,
					 friendRequest: notifications.friendRequest
                 });
               }
          });
  } else {
    return res.redirect('/');
  }
};

exports.postBoard = function(req, res, next) {
  if( req.session.email ){

	  var message   = sanitize(req.body.message_board);
	  var dateGT    = new Date().getTime();
	  var date      = dateFormat.getDate();
	  var email     = req.session.email;
	  var _id       = req.session._id;
	  var nickname  = req.session.nickname;
	  var firstname = req.session.firstname;
	  var lastname  = req.session.lastname;
	  var user      = sanitize( req.body.user );
	  var newUUIDv4 = uuidv4();

	  console.log('DATE:: ', date, dateGT);
	  console.log('MESSAGE:: ', message);
	  console.log('EMAIL:: ', email);
	  console.log('NICKNAME:: ', nickname);
	  console.log('_ID:: ', _id);
	  console.log('UUID:: ', newUUIDv4);
	  console.log('PARAMS::', req.params);
	  console.log('BODY:: ', req.body);
 // Users.findByIdAndUpdate({ '_id': req.session._id }
	  Users.findOneAndUpdate({ 'nickname': user }, { $push: { "messages_board": { date: dateFormat.getDate(), dateGT: dateGT, message: message, user:nickname, _id: _id, uuid: newUUIDv4 } } }, { safe: true, upsert: true, new: true },
		  function(err, results) {
			  if (err) return res.send({ error: err })
			  if (results) {
				  console.log(req.session.email);
				  console.log(results);
				  console.log(err);

				  return res.send({
					  response: 'Success !',
					  message : message,
					  user    : nickname,
					  uuid    : newUUIDv4,
					  date    : date
				  })
			  }
		  }
	  );
  } else {
    return res.redirect('/');
  }
};

exports.postDeleteMessage = function(req, res, next){
   if ( req.session.email ) {
      	var uuid = sanitize( req.body.uuid );
 		console.log(colors.red.bold('UUID:: '), uuid);

	  Users.findOneAndUpdate({
				  "messages_board.uuid": uuid
		   	},
			{
				$pull: { "messages_board": { "uuid": uuid } }
			},
			{
				safe: true, upsert: true, new: true
			},
		   function(err, data){
			  if( err ) {
				 return res.send('error')
			  }
			  if( data ){
				  console.log('success:: ', data);
				  return res.json({ message: "Message deleted !", uuid: uuid})
			  }
	  })
   }
}

/**
 * MEMBERS
 */
 exports.getMembers = function(req, res, next){
   if ( req.session.email ) {

        //////////////////////////////////////////
              Users.aggregate( [
                      { $project :
                        {  _id: 0, nickname: 1, friends: 1, avatar: 1, uid: 1  }
                      }
                ])
              .exec(function(err, results){

                      var data = [];

                      ///////////////////////////
                      for (var obj in results){
                              var friends = results[obj].friends;
                              var name = results[obj].nickname;

                              /**
                               * check if isFriend
                               */
                              function checkFriend(){
                                if( name != req.session.nickname ){
                                       if ( friends.indexOf( req.session.nickname) !== -1){
                                        return true;
                                      } else {
                                        return false
                                      }
                                  } else {
                                    return null
                                  }
                              };

                        //////////////////////////
                        data.push({
								  uid     : results[obj].uid,
                                  nickname: results[obj].nickname,
                                  isFriend: checkFriend(),
                                  avatar  : results[obj].avatar
                        });
                        //////////////////////////
                      };

                      //console.log('DATA:: ', data);
                      return res.render('members.ejs', {
                              user    : req.session,
                              session : req.session.nickname,
                              members : data,
							  friendRequest: notifications.friendRequest
                            });
              });
        //////////////////////////////////////////

     } else {
       res.redirect('/')
     }
 }


/**
 * Login
 **/
 exports.getProfile = function(req, res, next) {
   if ( req.session.email ) {
     return res.render("profile.ejs",  {
       user: req.session,
	   friendRequest: notifications.friendRequest,
         error: ""
     });
   } else {
     res.redirect('/');
   }
 };

exports.postProfile = function(req, res, next) {
  if (  req.session.email ) {
    return res.render("profile.ejs",  {
      user: req.session,
	  friendRequest: notifications.friendRequest,
        error: ""
    });
  } else {
    res.redirect('/');
  }
};

/**
 * info profile
 */
exports.getInfoProfile = function(req, res, next){
  if (  req.session.email ) {
    return res.render("info-profile.ejs",  {
      user: req.session,
	  friendRequest: notifications.friendRequest,
        error: ""
    });
  } else {
    res.redirect('/');
  }
}
/**
 * edit profile
 */
exports.getEditProfile = function(req, res, next){
  if ( req.session.email ) {
    return res.render("edit-profile.ejs",  {
      user: req.session,
	  friendRequest: notifications.friendRequest,
        error: ""
    });
  } else {
    res.redirect('/');
  }
}

/**
 * update profile
 */
exports.postUpdateProfile = function(req, res, next){
	if ( req.session.email ) {
          /**
           * vars
           */
          var sessionEmail = req.session.email;
          var email        = req.body['email'];
          var firstname    = req.body['firstname'];
          var lastname     = req.body['lastname'];
          var nickname     = req.body['nickname'];
          var password     = req.body['password'];
          var avatar       = req.file.filename || 'default_avatar.png';

          console.log(colors.green.bold('//////////////// UPDATE ////////////////'));
          console.log(colors.green.bold('req.body::'),req.body);
          console.log(colors.green.bold(sessionEmail, email, firstname, lastname, avatar));
          console.log(colors.green.bold('////////////////////////////////////////'));

          Users.findOne({
            'email': sessionEmail
          }, function(err, data){

            if(err && !data){
              ///////// err redirect ////////
              return res.redirect('/');
              ///////////////////////////////
            } else {
                  Users.update(

                     { _id: data._id },
                     { $set:
                        {
                          email        : email,
                          lastname     : lastname,
                          firstname    : firstname,
                          //nickname     : nickname,
                          dob          : dob,
                          gender       : gender,
                          presentation : presentation,
                          details      : details,
                          //password     : password,
                          avatar       : avatar
                        }
                     }, function(){
                            // set new property of the update
                            req.session.email        = email;
                            req.session.firstname    = firstname;
                            req.session.lastname     = lastname;
                           // req.session.nickname     = nickname;
                            req.session.dob          = dob;
                            req.session.gender       = gender;
                            req.session.presentation = presentation;
                            req.session.details      = details;
                           // req.session.password     = password;
                            req.session.avatar       = avatar;

                          return res.render('profile.ejs', {
                                  user    : req.session,
                                  session : req.session.nickname,
                                  avatar  : req.session.avatar,
								  friendRequest: notifications.friendRequest
                                });
                            //////////////////////////////////////////////////////////////////////////
                          });
                    }
              });
           };
}

/**
 * Friend-request
 */
 exports.getFriendRequest = function(req, res, next){
 	if ( req.session.email ) {

/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////

				Users.findOne({ _id: req.session._id },
				          		{ _id: 0} )
				    .select( 'friend_request' )
				    .exec(function(err, results){

						var obj = notifications.friendRequest.toObject();

						var uidArray = [];

						for(var i in obj){
							 uidArray.push( obj[i].uid );
						}
						console.log('uidArray', uidArray);

						Users.find({
							 uid: {$in: uidArray}} // -> $in select all occurence in array
						 )
						.exec(function(err, data) {

							console.log("USER:: ", data);
							console.log("notifications.friendRequest:: ", notifications.friendRequest);

							var avatarArray=[];

							data.forEach( function( value, index){

								avatarArray.push( value.avatar );
														///////////////////////////////////////////////////
														// notifications.friendRequest.toObject().forEach( function( val, i){
														// 	console.log("VAL2", val.user);
														//
														// 	console.log("VAL3", i);
														// 	console.log("AVATAR", value.avatar);
														//
														// 	setTimeout(function(){console.log("AVATAR::", notifications.friendRequest); },2000);
														// });
														//////////////////////////////////////////////////
							})

							return res.render('friend-request.ejs',{
							   error            : "",
							   user             : req.session,
							   //friendRequest    : results,
							   friendRequest    : notifications.friendRequest,
							   avatarArray      : avatarArray,
							   sendFriendRequest: null
						   })

						})

			      })

  	}
}

exports.postFriendRequest = function(req, res, next){
   if ( req.session.email ) {

	   var nickname = sanitize( req.body.nickname );
	   var uid      = sanitize( req.body.uid );
	   var date     = new Date().getTime();

	   console.log(colors.bold.green('REQUEST-FRIEND:: '), nickname, uid );

	   Users.findOneAndUpdate({
		//    "nickname": nickname,
		   "uid": uid
	}, { $addToSet: {"friend_request": {  user: req.session.nickname, uid: req.session.uid, date: date} } }, { safe: true, upsert: true, new: true},
	function(err, data){
		   if(err){
			   return res.json({ error:"Oops! Something went wrong :( Please try again)"})
		   }
		   if(data){
			   console.log( colors.bold.magenta("DATA:: "), data );
			   return res.json( { success: "You request has been sent !" })
		   }
	   })

   }
}

/**
 * response-friend-request
 */
exports.postResponseFriendRequest = function(req, res, next){
  	if ( req.session.email ) {
		var response = sanitize( req.body.response );
		var uid      = sanitize( req.body.uid );
		var nickname = sanitize( req.body.nickname );

			if( response == 'accept'){
					//console.log( response );

			Users.findOneAndUpdate({
					  "_id": req.session._id
			   },
			   	{ $addToSet: {"friends": nickname },
 				 		$pull: { "friend_request" : { "uid": uid } } },
				{ safe: true, upsert: true, new: true},
			   function(err, data){
					  if(err){
						  return res.json({ error:"Oops! Something went wrong :( Please try again)"})
					  }
					  if(data){

										///////////////////////////////// update friend ////////////////////////////////
										Users.findOneAndUpdate({
														  "uid": uid
												   },
												   { $addToSet: {"friends": req.session.nickname }},
												   //{ $unset: { "friend_request": uid }},
												   { safe: true, upsert: true, new: true},

												   function(err, data){
													   			console.log( colors.bold.magenta("DATA:: "), data );
														  	return res.json({ message: nickname + " and you are now friends !", icon:"ok"})
							     	   });
										///////////////////////////////////////////////////////////////////////////////

					  }
				  })

			}
			else if( response == 'decline'){
					//console.log( response );
					Users.findOneAndUpdate({
							  "_id": req.session._id
					   },
						{ $pull: { "friend_request" : { "uid": uid } } },
						{ safe: true, upsert: true, new: true},
					   function(err, data){
							  if(err){
								  return res.json({ error: "Oops! Something went wrong :( Please try again)"})
							  }
							  if(data){

												///////////////////////////////// update ///// ////////////////////////////////

												///////////////////////////////////////////////////////////////////////////////

												return res.json({ message: " Friend request by " + nickname + " declined !", icon:"remove"})
							  }
						  })
			}
			else if ( response !='accept' && response !='decline' ) {
					//console.log( response );
					return res.json({ message:"An error as occured"});
			}
		//console.log('RES:: ', response, uid, nickname);



	} else{
		return res.redirect('/');
	}
}

/**
 * search
 */
 exports.postSearch = function(req, res, next){

	 if( req.session ){

		 console.log('SEARCH REQ.BODY.QUERY', req.body.search);
	  	 var search = sanitize(  req.body.search );

		 Users.find(
				 {
					 "$or": [
					 	{ "nickname": { "$regex": search, "$options": "gim" } },
					 	{ "messages_board.message":  { "$regex": search, "$options": "gim"  } }
	 		 		]
				}
				,{
					_id                     : 0,
					"nickname"              : 1,
					"avatar"                : 1,
				    "messages_board"        : 1
				}
		).exec(function( err, data){
			 if(err) return res.json(err);

			 if( data ){
				 	console.log("DATA::----------------------------------------------------------- ");
					console.log(colors.bold.green("DATA.LENGTH--"), data.length);
					console.log(colors.bold.green("DATA--"), data);
					///////////////////////////////////////////
					///////////////////////////////////////////
					var results={ member: [], messages: [] };
					///////////////////////////////////////////
					///////////////////////////////////////////

					if( data.length == 0 ){
						return res.render('search.ejs', {
							user   : req.session,
							search : search,
							results: results
						})
					}

					var parseData = JSON.parse( JSON.stringify( data ));
					var regex = new RegExp( search, 'gim');

					parseData.forEach( function(val, i){

						////////////////////////////////////////////////////////
						var regexMatchInNickname = val.nickname.match( regex );
						console.log(colors.bold.green( "MATCH in nickname:: " ), regexMatchInNickname );
						///////////////////////////////////////////////////////

						if( regexMatchInNickname ){
					  			try{
									var member = { nickname: val["nickname"], avatar: val['avatar']};
								   results.member.push( member );
								   console.log( colors.bold.green( "RESULTS.MEMBER" ), results.member );

								   if( val.messages_board.length == 0 ){
									     console.log('1.:: LENGTH == LENGTH ');
									   for (var i in member){
										//   if( member[i].length == data.length){
											   console.log('2:: LENGTH == LENGTH ', member[i]);
											   render();
										   }
									  // }
								   }


								} catch(err){
									console.log(colors.bold.red( 'ERROR' ), err);
								}
						}

//
				if( val.messages_board.length > 0 ){	render();
// 					console.log(colors.bold.green("----> i :: "), val.messages_board,colors.bold.green('LENGTH:: '), val.messages_board.length );
//
// 						for( var i in val.messages_board){
// 							try{
//
// 						 		////////////////////////////////////////////////////////
// 								var regexMatchInMessage = val.messages_board[i].message.match( regex );
// 								console.log(colors.green.bold( "MATCH in message:: " ), regexMatchInMessage );
// 								///////////////////////////////////////////////////////
//
// 									if( regexMatchInMessage ){
// console.log(colors.bold.cyan( 'regexMatchInMessage') );
// 				  					results.messages.push( val.messages_board[i] );
// console.log(colors.bold.cyan( 'val.messages_board.length::'), val.messages_board.length, colors.bold.cyan('i'), i );
// console.log(colors.bold.magenta( 'i == val.messages_board.length -1 '), val.messages_board.length -1 );
// 				                        if( i == val.messages_board.length ) {
// console.log(colors.bold.cyan( 'i == val.messages_board.length -1 ') );
// 				                            console.log("MESSAGE ::", results );
// 					                          ////////////////////////////////////////////////////
// 					                          return res.render('search.ejs', {
// 					                                    user   : req.session,
// 														search : search,
// 					                                    results: results
//
// 													});
// 					                          ////////////////////////////////////////////////////
// 				                        }
// 								}
//
//
//
// 							}catch(err){
// 								console.log(colors.bold.red('ERROR:: '), err );
// 							};
//
// 						} // end for
					} // end if
					// else
					// {
					// 	render();
					// }


					function render(){

						console.log("ELSE:: ------------------------------");
								return res.render('search.ejs', {
									  user   : req.session,
									  search : search,
									  results: results
								  });
					}
				})
			}
	  	});
	///////////////////////

	 } else {
		 return res.redirect('/');
	 }



 }

/**
 * disconnect
 */
exports.getLogout = function(req, res, next){

    if(req.session){

            req.session.destroy(function(err){
               if(err){
                 console.log(err);
               } else {
                 req.session = null;
                 console.log(colors.red('/////////////////////////////'));
                 console.log(colors.bgRed('SESSION::'),  req.session);
                 console.log(colors.red('/////////////////////////////'));

                 res.redirect('/login');
               }
       });
    } else {
      res.redirect('/');
    }
}
